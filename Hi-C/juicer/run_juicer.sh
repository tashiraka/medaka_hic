#!/bin/bash

juicer_dir=$1
genome_path=$2
genome=$3
top_dir=$4

top_dir_name=`basename $top_dir`
current_dir=`pwd`
top_dir=${current_dir}/${top_dir_name}
bwa_index=${genome_path}/bwa_index/${genome}.fasta
chrom_size=${genome_path}/size/${genome}.fasta.chrom.sizes
re_sites=${genome_path}/re_sites/${genome}_MboI.txt
    
cd $top_dir
${juicer_dir}/scripts/juicer.sh -D $juicer_dir -d $top_dir -g $genome \
  -p $chrom_size -z $bwa_index -s MboI -y $re_sites -q all.q -l all.q -x
cd $current_dir
