#!/bin/bash

out_dir=hic
if [ ! -d ${out_dir} ]; then mkdir ${out_dir}; fi

# Rename all replicates for easy read
for ext in hic txt
do
    stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 st27 fibro)
    replicates=(rep1 rep2 mega)
    renamed_replicates=(rep1 rep2 combined)

    for st in ${stages[@]}
    do
        for rep_idx in ${!replicates[@]}
        do
            rep=${replicates[${rep_idx}]}
            renamed_rep=${renamed_replicates[${rep_idx}]}

            mv ${st}/${rep}/aligned/inter.${ext} ${out_dir}/${st}_${renamed_rep}.${ext}
            mv ${st}/${rep}/aligned/inter_30.${ext} ${out_dir}/${st}_${renamed_rep}_30.${ext}
        done
    done
done
