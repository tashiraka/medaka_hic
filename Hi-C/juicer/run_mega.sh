#!/bin/bash

juicer_dir=$1
genome_path=$2
genome=$3
stage_dir=$4

stage_dir_name=`basename $stage_dir`
current_dir=`pwd`
stage_dir=${current_dir}/${stage_dir_name}
chrom_size=${genome_path}/size/${genome}.fasta.chrom.sizes
re_sites=${genome_path}/re_sites/${genome}_MboI.txt

cd $stage_dir
${juicer_dir}/scripts/mega.sh -g $genome -s MboI -y $re_sites -x -p $chrom_size
cd $current_dir