#!/bin/bash

juicer_dir=$1 # Juicer version 1.5
juicer_tools=$2 # Juicer Tools version 1.5 for Arrowhead, HICCUPS, APA and dump.
                # Juicer Tools version 1.7.6 for Pearson's correlation matrices
                # and their eigenvectors.
juicer_tools_10=$3 # Juicer Tools version 1.5 for N10 statisics whose source
                   # codes of Arrowhead are modified to change the default
                   # filter size from 300kB to 50kB at 5kb resplution
                   # (60 to 10 bins).
jcuda_lib_path=$4 # JCUDA library path for HICCUPS
fastq_dir=$5 # containing the fastq files downloaded from DDBJ.
             # Assuming the file name pattern is
             # 'st(9|10|10.5|11|11.5|12|13|14|15|18|21|27|fibro)_rep(1|2)*.fastq(.gz)?'.
genome_path=`realpath ../genome/Hd-rR`
genome=medaka_hdrr2.2.4

#
# 1. Preprocess by Juicer
#
cd juicer
stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 st27 fibro)
replicates=(rep1 rep2)

for st in ${stages[@]}; do
    # Prepare files amd directories for Juicer
    if [ ! -d $st ]; then mkdir $st; fi
    for rep in ${replicates[@]}; do
        if [ ! -d ${st}/${rep} ]; then mkdir ${st}/${rep}; fi
        if [ ! -d ${st}/${rep}/fastq ]; then mkdir ${st}/${rep}/fastq; fi

        for fastq in `find $fastq_dir -type f -name "${st}_${rep}*.fastq.gz"`
        do
            f_name=`basename $fastq`
            if [ ! -f ${st}/${rep}/fastq/${f_name} ]; then
                ln -s $fastq ${st}/${rep}/fastq
            fi
        done   
    done

    # Run Juicer for each stage
    for rep in ${replicates[@]}; do
        bash run_juicer.sh $juicer_dir $genome_path $genome ${st}/${rep}
    done

    # Combining all replicates for each stage
    bash run_mega.sh $juicer_dir $genome_path $genome ${st}
done

# Move and rename the hic files
bash mv_and_rename.sh

cd ..

#
# 2. Hi-C analysis
#
cd juicer_tools

bash dump.sh $juicer_tools # Dump
bash pearsons.sh $juicer_tools # Pearsons correlation matrices
bash eigenvectors.sh $juicer_tools # Eigenvectors of Pearsons' matrices
bash arrowhead.sh $juicer_tools $juicer_tools_10 # Arrowhead
bash hiccups.sh $juicer_tools $jcuda_lib_path # HICCUPS
bash apa.sh $juicer_tools # APA
bash motif_finder.sh $juicer_tools $genome_path $genome # MotifFinder

cd ..

#
# 3. N10 statistics
#
cd DomainN10
bash n10.sh
cd ..