#!/bin/bash

stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 st27 fibro)

for st in ${stages[@]}
do
    name=${st}_combined_30_contact_domains_5000_10
    domain=../juicer_tools/arrowhead/${name}
    bed=ContactDomains/${name}_bed
    merged=MergedDomains/${name}_bed_sort_merged

    cat $domain | cut -f 1-3 > $bed
    sort -k1,1n -k2,2n -k3,3n $bed | bedtools merge -i - > $merged

    Rscript ContactDomaniN10.R
done