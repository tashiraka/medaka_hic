#!/bin/bash

juicer_tools=$1

out_dir=dump
if [ ! -d $out_dir ]; then mkdir $out_dir; fi
log=dump.log

# For loop filtering
chroms=(`seq 24`)
stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 st27 fibro)
resolusions=(5000 10000 25000 50000 100000)
res_names=(5kb 10kb 25kb 50kb 100kb)
replications=(rep1 rep2 combined)

for res_idx in ${!resolusions[@]}
do
    res=${resolusions[${res_idx}]}
    res_name=${res_names[${res_idx}]}

    for st in ${stages[@]}
    do
        for rep in ${replications[@]}
        do
            for chrom in ${chroms[@]}
            do
                hic=../juicer/hic/${st}_${rep}_30.hic                
                observed_kr=${st}_${rep}_30_${res_name}_observed_KR_${chrom}_${chrom}.txt
                kr_normalization_vector=${st}_${rep}_30_${res_name}_norm_KR_${chrom}.txt
                
                qsub -S /bin/bash -V -cwd -l mem_free=8g -o ${log} -e ${log} \
                     -j y -r y -N ${st}_${rep}_${chrom}_${res} <<EOF
#!/bin/bash
java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools dump observed KR $hic $chrom $chrom BP $res ${out_dir}/${observed_kr}
java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools dump norm KR $hic $chrom $chrom BP $res ${out_dir}/${kr_normalization_vector}
EOF
            done
        done
    done
done
