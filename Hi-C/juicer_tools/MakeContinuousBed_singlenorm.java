import java.io.*;

public class MakeContinuousBed_singlenorm {
	public static void main(String[] args) {

		String filename1 = args[0];// genome table
        String st = args[1];
        int res = Integer.valueOf(args[2]);
		String filenameOUT = "hiccups"+st+"_combined_30_5kb_norm_KR_all";

		try {			
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filenameOUT)));

            BufferedReader reader1  = new BufferedReader(new FileReader(new File(filename1)));
            String line1;
            while ((line1=reader1.readLine()) != null) {
                String chr = line1.split("\t")[0];
                String filename_norm = "dump"+st+"_combined_30_5kb_norm_KR_"+chr+".txt";
                BufferedReader reader_norm = new BufferedReader(new FileReader(new File(filename_norm)));
                String line_norm;
                
                System.out.println("Processing "+chr);
                int n = Integer.valueOf(line1.split("\t")[1])/res;
                for (int k=0;k<=n;k++) {
                    line_norm = reader_norm.readLine();
                    if (line_norm.equals("NaN")) {
                        line_norm = "0";
                    }
                    writer.println(chr+"\t"+(k*res)+"\t"+((k+1)*res)+"\t"+line_norm);
                }
                reader_norm.close();
            }
            reader1.close();
			writer.close();
		

		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
