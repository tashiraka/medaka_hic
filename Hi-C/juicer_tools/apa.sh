#!/bin/bash

juicer_tools=$1

hiccups_dir=hiccups
apa_dir=apa
if [ ! -d ${apa_dir} ]; then mkdir ${apa_dir}; fi

stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 st27 fibro)

for st in ${stages[@]}
do
    hic=../juicer/hic/${st}_combined_30.hic
    loop=${hiccups_dir}/${st}_combined_30_merged_loops
    out_dir=${apa_dir}/${st}
    if [ ! -d $out_dir ]; then mkdir $out_dir; fi
    log=${out_dir}/${st}_combined_30_apa.log

    qsub -S /bin/bash -cwd -V -o $log -e $log -N ${st}_apa -j y -r y <<APA
#!/bin/bash
java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools apa -r 5000 $hic $loop $out_dir
java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools apa -r 10000 $hic $loop $out_dir
APA
done

stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 st27)
stage2=fibro
suffix=sort_chronly_filtered

for st in ${stages[@]}
do
    hic=../juicer/hic/${st}_combined_30.hic
    loop=${hiccups_dir}/${stage2}_combined_30_merged_loops_${suffix}
    out_dir=${apa_dir}/${st}_with_${stage2}_loops_${suffix}
    if [ ! -d $out_dir ]; then mkdir $out_dir; fi
    log=${out_dir}/apa.log
    
    qsub -S /bin/bash -cwd -V -o $log -e $log -N ${st}_${stage2}_${suffix}_apa -j y -r y <<APA
#!/bin/bash
java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools apa -n 20 -w 6 -q 4 -r 5000 $hic $loop $out_dir
APA
done


