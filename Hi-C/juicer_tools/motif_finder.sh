#!/bin/bash

juicer_tools=$1
genome_path=$2
genome=$3
chrom_size=${genome_path}/size/${genome}.fasta.chrom.sizes

if [ ! -d motifFinder ]; then
    # Prepare directories
    mkdir motifFinder
    mkdir motifFinder/bed
    mkdir motifFinder/bed/inferred
    mkdir motifFinder/bed/unique

    # Prepare CTCF peaks
    raw_bed=../CTCF_ChIP-seq/st27_combined_CTCF.narrowPeak    
    bed=st27_combined_CTCF.narrowPeak_addChr.bed
    
    if [ -e $raw_bed ]; then
        cat $raw_bed | awk '{print "chr"$0}' > motifFinder/bed/inferred/${bed}
        cp motifFinder/bed/inferred/${bed} motifFinder/bed/unique
    else
        echo "CTCF bed file not found"
        exit 1
    fi
    
    # Prepare chromosomes sizes
    ln -s $chrom_size motifFinder/

    # Prepare Hiccups peaks
    loop=../hiccups/fibro_combined_30_merged_loops_sort_chronly_filtered
    
    if [ -e $loop ]; then
        ln -s $loop motifFinder/
    else
        echo "Loop file not found"
        exit 1
    fi

    # Prepare CTCF motifs
    motif=${genome_path}/motif/fimo_CTCF_1e-4.txt

    if [ -e $motif ]; then
        ln -s $motif motifFinder/
    else
        echo "CTCF motif file not found"
        exit 1
    fi

    # MotifFinder
    cd MotifFinder
    chrom_size=$(basename ${chrom_size})
    motif=$(basename ${motif})
    loop=$(basename ${loop})
    java -Djava.awt.headless=true -Xmx16000m -jar $juicer_tools motifs ${chrom_size} bed ${loop} ${motif}
    cd ..
    
else
    echo "$name exists."
    exit 1
fi
