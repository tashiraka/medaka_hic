#!/bin/bash

juicer_tools=$1
jcuda_lib_path=$2

hiccups_dir=hiccups
if [ ! -d $hiccups_dir ]; then mkdir $hiccups_dir; fi

stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 sst27 fibro)

for st in ${stages[@]}
do
    hic=../juicer/hic/${st}_combined_30.hic
    out_dir=${hiccups_dir}/${st}
    if [ ! -d $out_dir ]; then mkdir $out_dir; fi
    log=${out_dir}/${st}_combined_30_hiccups.log

    java -Djava.library.path=$jcuda_lib_path -Djava.awt.headless=true -Xmx16000m -jar $juicer_tools hiccups $hic $out_dir 2>&1 | tee $log
    mv ${out_dir}/merged_loops ${hiccups_dir}/${st}_combined_30_merged_loops

    # Filter loops
    java MakeContinuousBed_singlenorm ../../genome/Hd-rR/size/medaka_hdrr2.2.4.fasta.chrom.sizes_only_chroms $st 5000
    sort -k1,1n -k2,2n -k5,5n ${hiccups_dir}/${st}_combined_30_merged_loops > ${hiccups_dir}/${st}_combined_30_merged_loops_sort
    cat ${hiccups_dir}/${st}_combined_30_merged_loops_sort \
      | awk '$1 ~ /^([1-9]|1[0-9]|2[0-4])$/' \
      > ${hiccups_dir}/${st}_combined_30_merged_loops_sort_chronly
    Rscript loopfiltering.R
done
