#!/bin/bash

juicer_tools=$1
juicer_tools_10=$2

arrowhead_dir=arrowhead
if [ ! -d $arrowhead_dir ]; then mkdir $arrowhead_dir; fi

stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 st27 fibro)

for st in ${stages[@]}
do
    hic=../juicer/hic/${st}_combined_30.hic
    out_dir=${arrowhead_dir}/${st}
    if [ ! -d $out_dir ]; then mkdir $out_dir; fi
    log=${out_dir}/${st}_combined_30_arrowhead.log

    qsub -S /bin/bash -cwd -V -o $log -e $log -N ${st}_arrowhead -j y -r y <<ARROWHEAD
#!/bin/bash

java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools arrowhead -r 5000 $hic $out_dir
java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools arrowhead -r 10000 $hic $out_dir

cp ${out_dir}/5000_blocks ${arrowhead_dir}/${st}_combined_30_contact_domains_5000
cp ${out_dir}/10000_blocks ${arrowhead_dir}/${st}_combined_30_contact_domains_10000
ARROWHEAD

    # Arrowhead for N10 statistics
    out_dir_10=${arrowhead_dir}/${st}_10
    if [ ! -d $out_dir_10 ]; then mkdir $out_dir_10; fi
    log_10=${out_dir_10}/${st}_combined_30_arrowhead_10.log

    qsub -S /bin/bash -cwd -V -o $log_10 -e $log_10 -N ${st}_arrowhead_10 -j y -r y <<ARROWHEAD10
#!/bin/bash

java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools_10 arrowhead -r 5000 $hic $out_dir_10
cp ${out_dir_10}/5000_blocks ${arrowhead_dir}/${st}_combined_30_contact_domains_5000_10
ARROWHEAD10

done
