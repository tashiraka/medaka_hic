#!/bin/bash

juicer_tools=$1

out_dir=eigenvectors
if [ ! -d ${out_dir} ] then; mkdir ${out_dir}; fi
log=eigenvectors.log

chroms=(`seq 24`)
stages=(st9 st10 st10.5 st11 st11.5 st12 st13 st14 st15 st18 st21 st27 fibro)
resolutions=(50000)
res_names=(50kb)
replications=(rep1 rep2 combined)

for res_idx in ${!resolutions[@]}
do
    res=${resolutions[${res_idx}]}
    res_name=${res_names[${res_idx}]}

    for st in ${stages[@]}
    do
        for rep in ${replications[@]}
        do
            hic=../juicer/hic/${st}_${rep}_30.hic
            eigenvectors=${st}_${rep}_30_${res_name}_eigenvector_KR_${chrom}_${chrom}.txt
            for chrom in ${chroms[@]}
            do
                qsub -S /bin/bash -V -cwd -l mem_free=8g -o ${log} -e ${log} \
                     -j y -r y -N ${st}_${rep}_${chrom}_${res} <<EOF
#!/bin/bash
java -jar -Djava.awt.headless=true -Xmx16000m $juicer_tools eigenvector -p KR $hic $chrom BP $res ${out_dir}/${eigenvectors}
EOF
            done
        done
    done
done
