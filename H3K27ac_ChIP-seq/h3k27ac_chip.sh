#!/bin/bash

trimmomatic_path=$1 # Trimmomatic version 0.32
fastqs=${@:2} # downloaded from DDBJ. Assuming the file name pattern is
              # 'st(9|10|10.5|11|11.5|12)_(H3K27ac|input).fastq(.gz)?'
bwa_idnex=../genome/Hd-rR/bwa_index/medaka_hdrr2.2.4.fasta


for fastq in ${fastqs[@]}
do
    fastq_trim=${fastq%.*}_trim.fastq

    # Trim
    java -jar ${trimmomatic_path}/trimmomatic-0.32.jar SE -threads 6 -phred33 $fastq $fastq_trim \
         ILLUMINACLIP:${trimmomatic_path}/adapters/TruSeq3-SE.fa:3:40:15 LEADING:20 TRAILING:20 MINLEN:20

    # Mapping
    sai=${fastq_trim%.*}.sai
    sam=${sai%.*}.sam
    bam=${sam%.*}_q20.bam
    bam_sort=${bam%.*}_sort.bam
    bed=${bam%.*}.bed

    bwa aln $bwa_idnex $fastq > $sai
    bwa samse $bwa_index $sai $fastq_trim > $sam
    samtools view -bS -q 20 $sam > $bam
    samtools sort $bam  ${bam_sort%.*}
    bamToBed -i $bam_sort > $bed
done


stages=(st9 st10 st10.5 st11 st11.5 st12)
for st in ${stages[@]}
do
    macs2 callpeak -t ${st}_H3K27ac_trim_q20_sort.bed -c ${st}_input_trim_q20_sort.bed \
      -f BED -g 600000000 -n ${st}_H3K27ac -q 0.01 -B --SPMR
done
