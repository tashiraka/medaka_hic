import java.io.*;
public class LoopsToBed_loopsize {
	public static void main(String[] args) {

		String filename1 = args[0];//
        String filenameOut = args[1];

		try {
			BufferedReader reader1  = new BufferedReader(new FileReader(new File(filename1)));
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filenameOut)));

			String line;
			while ((line = reader1.readLine()) != null) {
                //chr1    x1    x2    chr2    y1    y2    color    observed    expectedBL    expectedDonut    expectedH    expectedV    fdrBL    fdrDonut    fdrH    fdrV    numCollapsed    centroid1    centroid2    radius
                //10    11135000    11140000    10    11175000    11180000    0,255,255    49.0    21.112894    21.916811    22.916546    22.974096    0.0035740796    0.0031218936    0.005363805    0.0058618383    4    11132500    11178750    13288
                if (line.charAt(0)=='c') {
                } else {
                    String[] L = line.split("\t");
                    int loopsize = Integer.valueOf(L[4]) - Integer.valueOf(L[1]);
                    writer.println(L[0]+"\t"+L[1]+"\t"+L[2]+"\t"+loopsize);
                    writer.println(L[3]+"\t"+L[4]+"\t"+L[5]+"\t"+loopsize);
                }
			}
    
			reader1.close();		
			writer.close();
			
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
