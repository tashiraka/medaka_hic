import java.io.*;
import java.util.ArrayList;

class Record_region{
	String protein; 
	String gene;
	Record_region(String n, String x) {
		protein = n;
		gene = x;
	}
	Record_region(){;}
}
class Hash_chr{
	static int BucketSize;
	static ArrayList[] Bucket;
	
	Hash_chr(int size){
		BucketSize = size;
		Bucket = new ArrayList[BucketSize];
		for(int n=0;n<BucketSize;n++){
			Bucket[n] =  new ArrayList();//
		}
	}

	static int C2I(char c){
		return c-' '+1;
	}
	
	static int LinearProblingHashValue(String s){
		int h =0;
		int length = s.length();
		for(int n=0;n<length;n++){
			int v =C2I(s.charAt(n));
			h = (32*h+v)%BucketSize;
		}
		return h;
	}
	
	public void LinearProblingInsert(String n, String x){
		int h;
		h = LinearProblingHashValue(n);
		Bucket[h].add(new Record_region(n,x));
	}
	
	public Record_region LinearProblingSearch(String k){
		int h;int flag=0;
		h = LinearProblingHashValue(k);
		int num = Bucket[h].size();
		Record_region tmp =null;
		
		for(int i=0;i<num;i++){
			tmp = (Record_region)Bucket[h].get(i);
			if(tmp.protein.compareTo(k)==0){
				flag=1;												
				break;}
		}					
		if(flag==1)
			{
			return tmp;					    
			}		
		else
			return null;
	}
}
public class MedakaV2GeneID_withENSORLP {
	public static void main(String[] args) {

		String filename1 = args[0]; //Medaka-gene-ver2.2.4.fasta_geneID_ENS_tss
		String filename2 = args[1]; //
		String filename3 = args[1]+"_wENSid";


		try {
			BufferedReader reader1  = new BufferedReader(new FileReader(new File(filename1)));
			
			int BucketSize=500009;
			Hash_chr H = new Hash_chr(BucketSize);
			String line = "";
			while ((line = reader1.readLine()) != null) {
				//15    25698889    25698890    MEDAKA000002    ENSORLP00000012727
                String geneID = line.split("\t")[3];
                String proteinID = line.split("\t")[4];
                H.LinearProblingInsert(geneID,proteinID);
            }
			reader1.close();
			
			BufferedReader reader2 = new BufferedReader(new FileReader(new File(filename2)));
            PrintWriter writer1 = new PrintWriter(new BufferedWriter(new FileWriter(filename3)));

			int in = 0;
			String header = "";
            int col = 0;
			while ((line = reader2.readLine()) != null) {
                //geneID    count_st9_rep1    count_st10_rep1    count_st11_rep1    count_st12_rep1    count_st13_rep1    count_st14_rep1    count_st15_rep1    count_st18_rep1    count_st21_rep1    count_st27_rep1
                //MEDAKA000001    12    14    26    8    4    1    2    0    0    2
                //MEDAKA000002    0    0    2    32    7    2    1    0    0    15
                if (line.charAt(0)=='g') {
                    writer1.println(line);
                } else {
                    String[] m = line.split("\t");
                    String gID = m[0];
                    Record_region answer = H.LinearProblingSearch(gID);
                    if (answer != null) {
                        writer1.println(line);
                    }
                }
			}		
			reader2.close();
			writer1.close();

		} catch (IOException e) {
			System.out.println(e);
		}
	}
}

