#!/bin/bash

# RNA-seq was mapped by STAR.
# A. Dobin et al., STAR: Ultrafast universal RNA-seq aligner. Bioinformatics. 29, 15–21 (2013).
# The software is aveilable at GitHub https://github.com/alexdobin/STAR

# Trimming the requenced reads by Trimomatic
# Bolger, A. M., Lohse, M., & Usadel, B. (2014). Trimmomatic: A flexible trimmer for Illumina Sequence Data. Bioinformatics, btu170.
# The software is available at http://www.usadellab.org/cms/?page=trimmomatic

trimmomatic_path=$1 # Trimmomatic version 0.32
rna_fastq_dir=$2 # downloaded from DDBJ. Assuming the file name pattern is
                 # 'RNA_st(9|10|11|12|13|14|15|18|21|27)_rep(1|2).fastq(.gz)?'

star_idx=../genome/Hd-rR/star_index
stages=(st9 st10 st11 st12 st13 st14 st15 st18 st21 st27)
replicates=(rep1) # (rep1 rep2)

# Mapping
for st in ${stages[@]}
do
    for rep in ${replicates[@]}
    do
        # If fastq files were separated, concatenate them first.
        # Here, the concatenated file is named RNA_${st}_${rep}.fastq
        prefix=RNA_${st}_${rep}
        fastq=${prefix}.fastq
        trimmed=${prefix}_trim.fastq

        # Trimming
        ${trimmomatic_path}/trimmomatic-0.32.jar SE -threads 6 -phred33 \
            ${rna_fastq_dir}/${fastq} $trimmed \
            ILLUMINACLIP:${trimmomatic_path}/adapters/TruSeq3-SE.fa:3:40:15 \
            LEADING:20 TRAILING:20 MINLEN:20

        # Mapping
        STAR --genomeDir $star_idx --runThreadN 10 --readFilesIn $trimmed --outFileNamePrefix $prefix

        # Filtering
        samtools view -bS -q 20 ${prefix}_Aligned.out.sam > ${prefix}_Aligned.out_q20.bam
        samtools sort -o ${prefix}_Aligned.out_q20_sort.bam ${prefix}_Aligned.out_q20.bam
        bedtools bamtobed -i ${prefix}_Aligned.out_q20_sort.bam > ${prefix}_Aligned.out_q20_sort.bed
    done
done

# Output count data
Rscript RPKM_v2.R

# Get genes ensembl ID
gene_ens=../genome/Hd-rR/gene/Medaka-gene-ver2.2.4.fasta_geneID_ENS_tss
java MedakaV2GeneID_withENSORLP $gene_ens RNA_counts_rep1.txt
java MedakaV2GeneID_withENSORLP $gene_ens RNA_RPKM_rep1.txt

# Get loop gene
gene_ens_sort=${gene_ens}_sort
sort -k1,1n -k2,2n $gene_ens > $gene_ens_sort

loops=../Hi-C/juicer_tools/hiccups/fibro_combined_v2genome_30_merged_loops_sort_chronly_filtered
loops_bed=fibro_combined_v2genome_30_merged_loops_sort_chronly_filtered_bed_lpsize
loops_bed_sort=fibro_combined_v2genome_30_merged_loops_sort_chronly_filtered_bed_lpsize_sort
java LoopsToBed_loopsize $loops $loops_bed
sort -u -k1,1 -k2,2n $loops_bed > $loops_bed_sort

loops_gene_ens=fibro_combined_v2genome_30_merged_loops_sort_chronly_filtered_bed_lpsize_sort_genesENS
bedtools closest -d -t all -a $loops_bed_sort -b $gene_ens_sort > $loops_gene_ens
java ProteinID_To_GeneID_loopsize ../genome/Hd-rR/gene/GeneID_ProteinID.txt fibro_combined_v2genome_30_merged_loops_sort_chronly_filtered_bed_lpsize_sort_genesENS

# Clustering transcription start timing
Rscript PAM_loopgenes_count_k7.R

