import java.io.*;
import java.util.ArrayList;

class Record_region{
	String protein; //10:2569938
	String gene;
	Record_region(String n, String x) {
		protein = n;
		gene = x;
	}
	Record_region(){;}
}
class Hash_chr{
	static int BucketSize;
	static ArrayList[] Bucket;
	
	Hash_chr(int size){
		BucketSize = size;
		Bucket = new ArrayList[BucketSize];
		for(int n=0;n<BucketSize;n++){
			Bucket[n] =  new ArrayList();//
		}
	}

	static int C2I(char c){
		return c-' '+1;
	}
	
	static int LinearProblingHashValue(String s){
		int h =0;
		int length = s.length();
		for(int n=0;n<length;n++){
			int v =C2I(s.charAt(n));
			h = (32*h+v)%BucketSize;
		}
		return h;
	}
	
	public void LinearProblingInsert(String n, String x){
		int h;
		h = LinearProblingHashValue(n);
		Bucket[h].add(new Record_region(n,x));
	}
	
	public Record_region LinearProblingSearch(String k){
		int h;int flag=0;
		h = LinearProblingHashValue(k);
		int num = Bucket[h].size();
		Record_region tmp =null;
		
		for(int i=0;i<num;i++){
			tmp = (Record_region)Bucket[h].get(i);
			if(tmp.protein.compareTo(k)==0){
				flag=1;												
				break;}
		}					
		if(flag==1)
			{
			return tmp;					    
			}		
		else
			return null;
	}
}
public class ProteinID_To_GeneID_loopsize {
	public static void main(String[] args) {

		String filename1 = args[0]; //GeneID_ProteinID
		String filename2 = args[1]; //
		String filename3 = args[1]+"_GeneID";


		try {
			BufferedReader reader1  = new BufferedReader(new FileReader(new File(filename1)));
			
			int BucketSize=500009;
			Hash_chr H = new Hash_chr(BucketSize);
			String line = "";
			while ((line = reader1.readLine()) != null) {
				//GeneID    ProteinID
				//ENSORLG00000000001    ENSORLP00000000001
                if (line.split("\t").length==2) {
                    String geneID = line.split("\t")[0];
                    String proteinID = line.split("\t")[1];
                    H.LinearProblingInsert(proteinID,geneID);
                }
            }
			reader1.close();
			
			BufferedReader reader2 = new BufferedReader(new FileReader(new File(filename2)));
            PrintWriter writer1 = new PrintWriter(new BufferedWriter(new FileWriter(filename3)));

			int in = 0;
			String header = "";
            int col = 0;
			while ((line = reader2.readLine()) != null) {
                //1    1300000    1310000    920000    1    1310393    1310394    MEDAKA054700    ENSORLP00000000034
                String[] m = line.split("\t");
				String pID = m[8];
				Record_region answer = H.LinearProblingSearch(pID);
                if (answer != null) {
                    writer1.println(line+"\t"+answer.gene);
                } else {
                    System.out.println("no Gene ID found for "+pID);
                }
			}		
			reader2.close();
			writer1.close();

		} catch (IOException e) {
			System.out.println(e);
		}
	}
}

