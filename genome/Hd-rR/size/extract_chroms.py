import sys
import pandas as pd


chrom_size_file = sys.argv[1]
out_file = sys.argv[2]

chrom_size = pd.read_table(chrom_size_file, header=None)
chroms = [str(i) for i in range(1, 24 + 1)]

fout = open(out_file, 'w')

for idx, row in chrom_size.iterrows():
    if row[0] in chroms:
        fout.write(row[0] + '\t' + str(row[1]) + '\n')
