#!/bin/bash

ln -s ../fasta/medaka_hdrr2.2.4.fasta .
samtools faidx medaka_hdrr2.2.4.fasta
rm medaka_hdrr2.2.4.fasta
cut -f1,2 medaka_hdrr2.2.4.fasta.fai > medaka_hdrr2.2.4.fasta.chrom.sizes
rm medaka_hdrr2.2.4.fasta.fai


python extract_chrom.py medaka_hdrr2.2.4.fasta.chrom.sizes medaka_hdrr2.2.4.fasta.chrom.sizes_only_chroms
