#!/bin/bash

#
# Cut the genome by restriction enzyme in-silico for Juicer
#
juicer_dir=$1 # path to Juicer directory
genome=../fasta/medaka_hdrr2.2.4.fasta

python ${juicer_dir}/misc/generate_site_positions.py MboI medaka_hdrr2.2.4 $genome
