#!/bin/bash

wget http://utgenome.org/medaka_v2/Medaka-Hd-rR-PacBio_version2.2.4.fasta.gz
wget http://utgenome.org/medaka_v2/Medaka-Hd-rR-PacBio_falcon_pilon_unanchored_version2.2.4.fasta.gz

zcat Medaka-Hd-rR-PacBio_version2.2.4.fasta.gz Medaka-Hd-rR-PacBio_falcon_pilon_unanchored_version2.2.4.fasta.gz > medaka_hdrr2.2.4.fasta
