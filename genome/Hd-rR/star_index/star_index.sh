#!/bin/bash


STAR --runMode genomeGenerate --runThreadN 10 \
     --genomeDir ./ --genomeFastaFiles ../fasta/medaka_hdrr2.2.4.fasta
