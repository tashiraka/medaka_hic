#!/bin/bash

name=medaka_hdrr2.2.4
fasta=../fasta/medaka_hdrr2.2.4.fasta

gem-indexer -i $fasta -o $name
gem-mappability -I ${name}.gem -l 50 -o ${name}.50mer
rdmd count_exclamation_mark.d ${name}.50mer.mappability > effective_length.txt
gem-2-wig -I ${name}.gem -i ${name}.50mer.mappability -o ${name}.50mer
