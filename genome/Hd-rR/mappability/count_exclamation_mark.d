import std.stdio;

void main(string[] args)
{
  immutable auto gemMappability = args[1];
  // e.g. gemMappability = "Medaka-Hd-rR_2.2.2.50mer.mappability"

  immutable ulong header = 109;
  ulong count = 0;
  ulong total = 0;
  ulong lineNum = 1;
  
  foreach (line; File(gemMappability).byLine) {
    if (lineNum > header && line[0] != '~') {
      foreach (c; line) {
        total++;
        if (c == '!') count++;
      }
    }
    lineNum++;
  }

  writeln("The effective length is # uniquely re-mapped positions.");
  writeln("This is appeared as a character '!'.");
  writeln("# !: ", count);
  writeln("# total: ", total);
  writeln("Ratio: ", 1.0 * count / total);
}
