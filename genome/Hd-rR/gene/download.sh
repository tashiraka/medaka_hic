#!/bin/bash

wget http://utgenome.org/medaka_v2/Medaka-gene-ver2.2.4.fasta.gz
wget http://utgenome.org/medaka_v2/Medaka-gene-ver2.2.4.gff.gz
gzip -d Medaka-gene-ver2.2.4.fasta.gz
gzip -d Medaka-gene-ver2.2.4.gff.gz

# Modify the format
java ModifyGFF Medaka-gene-ver2.2.4.gff

# Get genes with Ensembl gene ID
grep ">MEDAKA" Medaka-gene-ver2.2.4.fasta > Medaka-gene-ver2.2.4.fasta_geneID
grep "ENSORLP" Medaka-gene-ver2.2.4.fasta_geneID > Medaka-gene-ver2.2.4.fasta_geneID_ENS
java TSS_medakav2ID_ENS Medaka-gene-ver2.2.4.fasta_geneID_ENS

# Get the correspondence between Gene ID and Protein ID
wget -O GeneID_ProteinID.txt 'http://apr2018.archive.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "olatipes_gene_ensembl" interface = "default" ><Attribute name = "ensembl_gene_id" /><Attribute name = "ensembl_peptide_id" /></Dataset></Query>'

sed -i '1s/^/GeneID\tProteinID\n/' GeneID_ProteinID.txt
