import java.io.*;

public class ModifyGFF {
	public static void main(String[] args) {

		String filename1 = args[0];// Medaka-gene-ver2.2.4.gff
        String filenameOut = args[0]+"_geneID";

		try {
			BufferedReader reader1  = new BufferedReader(new FileReader(new File(filename1)));
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filenameOut)));

			String line;
			while ((line = reader1.readLine()) != null) {
                //1    maker    mRNA    73487    76721    .    -    .    ID=MEDAKA026675
                //1    maker    exon    73487    75665    .    -    .    ID=MEDAKA026675:exon:219;Parent=MEDAKA026675
                //1    maker    exon    76139    76275    .    -    .    ID=MEDAKA026675:exon:218;Parent=MEDAKA026675
                String[] G = line.split("\t");
                if (G.length>1) {
                    String type = G[2];
                    if (type.equals("mRNA")) {
                        String id = G[8].split("=")[1];
                        writer.println(line+";geneID="+id);
                    } else {
                        writer.println(line);
                    }
                } else {
                    writer.println(line);
                }
			}
    
			reader1.close();		
			writer.close();
			
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
