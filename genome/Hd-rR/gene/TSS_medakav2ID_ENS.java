import java.io.*;

public class TSS_medakav2ID_ENS {
	public static void main(String[] args) {

		String filename1 = args[0];//
        String filenameOut = args[0]+"_tss";

		try {
			BufferedReader reader1  = new BufferedReader(new FileReader(new File(filename1)));
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filenameOut)));

			String line;
			while ((line = reader1.readLine()) != null) {
				//>MEDAKA000002 15:25698889-25702894 + ENSORLP00000012727.1 gene_symbol:nrxn1b description:neurexin 1b [Source:ZFIN;Acc:ZDB-GENE-070206-3]
                String[] G = line.split(" ");
                String id = G[0].split(">")[1];
                String ENSPID = G[3].split("\\.")[0];
                String chr = G[1].split(":")[0];
                int start = Integer.valueOf(G[1].split(":")[1].split("-")[0]);
                int end = Integer.valueOf(G[1].split(":")[1].split("-")[1]);
                String strand = G[2];
                String name;
                if (strand.equals("+")) {
                    writer.println(chr+"\t"+start+"\t"+(start+1)+"\t"+id+"\t"+ENSPID);
                } else if (strand.equals("-")) {
                    writer.println(chr+"\t"+end+"\t"+(end+1)+"\t"+id+"\t"+ENSPID);
                } else {
                    System.out.println("error");
                }
			}
    
			reader1.close();		
			writer.close();
			
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
