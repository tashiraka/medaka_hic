#!/bin/bash

#
# Medaka genome version 2 can be downloaded from this page
# http://utgenome.org/medaka_v2/#!Assembly.md.
#

juicer_dir=$1 # path to juicer directory

###############################
# Hd-rR
###############################
cd Hd-rR

# 1. Download of the medaka reference genome v2
cd fasta
bash download.sh
bash faidx.sh
cd ..

# 2. Genome size
cd size
bash size.sh
cd ..

# 3. Mappability
cd mappability
bash calculate_effective_size.sh
cd ..

# 4. Restriction enzyme cut sites
cd re_sites
bash re_sites.sh $juicer_dir
cd ..

# 5. BWA index
cd bwa_index
bash bwa_index.sh
cd ..

# 6. STAR index
cd star_index
bash star_index.sh
cd ..

# 7. Genes
cd gene
bash download.sh
cd ..

# 8. Motif
cd motif
bash motif.sh
cd ..

# End of process
cd ..

###############################
# HNI
###############################
cd HNI

# 1. Download of the medaka reference genome v2
cd fasta
bash download.sh
cd ..

# 2. BWA index
cd bwa_index
bash bwa_index.sh
cd ..

# 3. STAR index
cd star_index
bash star_index.sh
cd ..

# End of process
cd ..

###############################
# Hybrid of Hd-rR and HNI
###############################
cd hybrid

# 1. Download of the medaka reference genome v2
cd fasta
bash download.sh
cd ..

# 2. STAR index
cd star_index
bash star_index.sh
cd ..
