#!/bin/bash

STAR --runMode genomeGenerate --genomeDir ./ --genomeFastaFiles ../fasta/medaka_hdrr_hni2.2.4.fasta --runThreadN 4
