#!/bin/bash

zcat \
../Hd-rR/fasta/Medaka-Hd-rR-PacBio_version2.2.4.fasta.gz \
../Hd-rR/fasta/Medaka-Hd-rR-PacBio_falcon_pilon_unanchored_version2.2.4.fasta.gz \
../HNI/fasta/Medaka-HNI-PacBio_version2.2.4.fasta.gz \
../HNI/fasta/Medaka-HNI-PacBio_falcon_pilon_unnchored_version2.2.4.fasta.gz \
> medaka_hdrr_hni2.2.4.fasta
                                                           
