#!/bin/bash

wget http://utgenome.org/medaka_v2/Medaka-HNI-PacBio_version2.2.4.fasta.gz
wget http://utgenome.org/medaka_v2/Medaka-HNI-PacBio_falcon_pilon_unanchored_version2.2.4.fasta.gz


zcat Medaka-HNI-PacBio_version2.2.4.fasta.gz Medaka-HNI-PacBio_falcon_pilon_unanchored_version2.2.4.fasta.gz > medaka_hni2.2.4.fasta
