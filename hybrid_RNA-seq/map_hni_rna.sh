#!/bin/bash


THREAD=4

data_dir=fastq

SAMP=${data_dir}/SRR3168578
F1TRIM=${data_dir}/SRR3168578.trim.fastq

##mapping by STAR
GDIR=../genome/hybrid/star_index

STAR --genomeDir ${GDIR} --readFilesIn ${F1TRIM} --runThreadN ${THREAD} --outSAMtype BAM SortedByCoordinate --outFileNamePrefix ${SAMP}

STARBAM=${SAMP}Aligned.sortedByCoord.out.bam

## filtering with mapQ>=10
STARFBAM=${SAMP}Aligned.sortedByCoord.out.q10.bam
samtools view -b -q 10 ${STARBAM} > ${STARFBAM}



intersectBed -v -F 1bp -a DRR002216_2217.mem.sort.rg.realign.bam.var.flt.chr.Q20.DP20.bed -b $STARFBAM > DRR002216_2217.mem.sort.rg.realign.bam.var.flt.chr.Q20.DP20.no-HNIblasturaRNAseq-mapped_wSTAR.bed
