#!/bin/bash

sample_names=$1 # Download the fastq files into hybrid_RNA-seq/fastq.
                # Assuming the file names are '*_(1|2).fastq'.
                # hybrid_rna_sample_names stores the '*' part of those names7.

data_dir=fastq
THREAD='2'


for SAMP_NAME in ${sample_names[@]}
do
    SAMP=${data_dir}/${SAMP_NAME}
    
    F1TRIM=${SAMP}.trim.fastq

    ##mapping by STAR
    GDIR=../genome/hybrid/star_index

    STAR --genomeDir ${GDIR} --readFilesIn ${F1TRIM} --runThreadN ${THREAD} --outSAMtype BAM SortedByCoordinate --outFileNamePrefix ${SAMP}

    STARBAM=${SAMP}.trim.star.sort.bam

    ## filtering with mapQ=10
    STARFBAM=${SAMP}.trim.star.sort.q10.bam
    samtools view -b -q 10 ${STARBAM} > ${STARFBAM}

    ## filtering with SNP site
    STARVBAM=${SAMP}.trim.star.sort.q10.var.bam
    BED=${data_dir}/DRR002216_2217.mem.sort.rg.realign.bam.var.flt.chr.Q20.DP20.no-HNIblasturaRNAseq-mapped_wSTAR.bed
    samtools view -b -L ${BED} ${STARFBAM} > ${STARVBAM}

    samtools index ${STARVBAM}
    samtools index ${STARFBAM}
done
