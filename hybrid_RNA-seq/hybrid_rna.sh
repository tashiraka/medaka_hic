#!/bin/bash

trimmomatic_path=$1 # Trimmomatic version 0.32
picard_path=$2 # Picard version 2.6.0
gatk_path=$3   # GATK version 3.6
sample_names=$4 # Download the fastq files into hybrid_RNA-seq/fastq.
                # Assuming the file names are '*_(1|2).fastq'.
                # hybrid_rna_sample_names stores the '*' part of those names.

# Download raw fastq data
bash download_raw_data.sh

# Call variants between Hd-rR and HNI strains of medaka
bash call_variants.s $trimmomatic_path $picard_path $gatk_path

# Mapping HNI RNA-seq reads to hybrid genome
bash map_hni_rna.sh

# Mapping hybrid RNA-seq reads to hybrid genome, and
# isolate paternal transcripts
bash map_rna_to_hybrid.sh ${sample_names[@]}
