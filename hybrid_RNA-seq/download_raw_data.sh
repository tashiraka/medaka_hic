#!/bin/bash


data_dir=fastq

if [ ! -d $data_dir ]; then mkdir $data_dir; fi

cd $data_dir
# HNI WGS to call variants
wget ftp://ftp.ddbj.nig.ac.jp/ddbj_database/dra/fastq/DRA000/DRA000588/DRX001641/DRR002216_1.fastq.bz2
wget ftp://ftp.ddbj.nig.ac.jp/ddbj_database/dra/fastq/DRA000/DRA000588/DRX001642/DRR002216_2.fastq.bz2
wget ftp://ftp.ddbj.nig.ac.jp/ddbj_database/dra/fastq/DRA000/DRA000588/DRX001642/DRR002217_1.fastq.bz2
wget ftp://ftp.ddbj.nig.ac.jp/ddbj_database/dra/fastq/DRA000/DRA000588/DRX001642/DRR002217_2.fastq.bz2

# RNA-seq data of HNI RNA-seq
wget ftp://ftp.ddbj.nig.ac.jp/ddbj_database/dra/sralite/ByExp/litesra/SRX/SRX158/SRX1584457/SRR3168578/SRR3168578.sra
fastq-dump ./SRR3168578.sra

cd ..
