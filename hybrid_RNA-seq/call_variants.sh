#!/bin/bash


trimmomatic_path=$1 # Trimmomatic version 0.32
picard_path=$2 # Picard version 2.6.0
gatk_path=$3   # GATK version 3.6

data_dir=fastq
REF_FASTA=../genome/Hd-rR/fasta/medaka_hdrr2.2.4.fasta
#../genome/Hd-rR/fasta/medaka_hdrr2.2.4.fasta.fai must exist.
REF_BWA=../genome/Hd-rR/bwa_index/medaka_hdrr2.2.4.fasta
DICT=../genome/Hd-rR/fasta/medaka_hdrr2.2.4.dict


sample_names=(DRR002216 DRR002217)
bams=()


for SAMP in ${sample_names[@]}
do
    R1=${data_dir}/${SAMP}_1.fastq
    R2=${data_dir}/${SAMP}_2.fastq
    BZ1=${R1}.bz2
    BZ2=${R2}.bz2

    bunzip2 ${BZ1}
    bunzip2 ${BZ2}
    
    R1TRIM=${data_dir}/${SAMP}_R1.trim.fastq
    R2TRIM=${data_dir}/${SAMP}_R2.trim.fastq
    R1SINGL=${data_dir}/${SAMP}_R1.single.fastq
    R2SINGL=${data_dir}/${SAMP}_R2.single.fastq
    TRIMLOG=${data_dir}/${SAMP}.trimlog


    #
    # Preprocess raw reads
    #
    TRUSEQ3=${trimmomatic_path}/adapters/TruSeq3-PE.fa
    ln -s ${TRUSEQ3}

    java -jar ${trimmomatic_path}/trimmomatic-0.32.jar PE ${R1} ${R2} ${R1TRIM} ${R1SINGL} ${R2TRIM} ${R2SINGL} \
         ILLUMINACLIP:${TRUSEQ3}:3:40:15 LEADING:20 TRAILING:20 MINLEN:20


    #
    # Mapping
    #
    SAM=${data_dir}/${SAMP}.mem.sam

    bwa mem -t 8 ${REF_BWA} ${R1TRIM} ${R2TRIM} > ${SAM}


    #
    # SAM to BAM
    #
    # samtools v1.2
    BAM=${SAM%.*}.bam
    samtools view -b -o $BAM $SAM
    bams+=(${BAM})
done


#
# Some processes to BAM files
#
bam_all=${data_dir}/DRR002216_2217.mem.bam
samtools cat -o $bam_all $bams

bam_all_sort=${data_dir}/DRR002216_2217.mem.sort.bam
samtools sort $bam_all ${bam_all_sort%.*}

samtools index $bam_all_sort


#
# Add read group
#
bam_rg=${bam_all_sort%.*}.rg.bam
INT=${bam_rg}.intervals

java -jar ${picard_path}/picard.jar AddOrReplaceReadGroups I=$bam_all_sort O=$bam_rg \
     RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=HNI

java -jar ${picard_path}/picard.jar CreateSequenceDictionary R=${REF_FASTA} O=${DICT}

samtools index $bam_rg


#
# Local Realignmnet by GATK
#
java -jar ${gatk_path}/GenomeAnalysisTK.jar -T RealignerTargetCreator -R ${REF_FASTA} -I $bam_rg -o ${INT}

REAL=${bam_rg%.*}.realign.bam
java -jar ${gatk_path}/GenomeAnalysisTK.jar -T IndelRealigner -R ${REF_FASTA} -I $bam_rg -targetIntervals ${INT} -o ${REAL}

samtools index ${REAL}


#
# Variant Calling. !!! NOTE fasta must be indexed by samtools faidx
#
RAWV=${REAL}.var.raw.vcf
FLTV=${REAL}.var.flt.vcf
samtools mpileup -q 20 -uf ${REF} ${REAL} | bcftools call -mv > ${RAWV}
bcftools filter -s LowQual -e '%QUAL<20 || DP>10' ${RAWV} > ${FLTV}

# Further filtering
FLTV_flt=${FLTV%.*}.chr.Q20.DP20.vcf
# vcfutils.pl can be installed with samtools. For example, it should be found in /usr/share/samtools/vcfutils.pl in Linux.
vcfutils.pl varFilter -d20 $FLTV | awk '$1 >= 1 && $1 <= 24 && $6 >= 20 {print}' > $FLTV_flt

FLTV_flt_bed=${FLTV_flt%.*}.bed
cat $FLTV_flt | awk '{print "Hd-rR_"$1"\t"$2"\t"$2}' > $FLTV_flt_bed


# $FLTV_flt_bed is the final output
