#!/bin/bash

trimmomatic_path=$1 # Trimmomatic version 0.32
fastqs=${@:2} # downloaded from DDBJ. Assuming the file name pattern is
              # 'st(9|10|11|12|13|14|15|18|21|27)_rep(1|2)_(CTCF|input).fastq(.gz)?'. 
bwa_idnex=../genome/Hd-rR/bwa_index/medaka_hdrr2.2.4.fasta


for fastq in ${fastqs[@]}
do
    fastq_trim=${fastq%.*}_trim.fastq

    # Preprocess
    java -jar ${trimmomatic_path}/trimmomatic-0.32.jar SE -threads 6 -phred33 $fastq_file $fastq_trim \
         ILLUMINACLIP:${trimmomatic_path}/adapters/TruSeq3-SE.fa:3:40:15 LEADING:20 TRAILING:20 MINLEN:20

    # Mapping
    sai=${fastq_trim%.*}.sai
    sam=${sai%.*}.sam
    bam=${sam%.*}_q20.bam
    bam_sort=${bam%.*}_sort.bam
    bed=${bam%.*}.bed

    bwa aln $bwa_index $fastq_trim > $sai
    bwa samse $bwa_index $sai $fastq_trim > $sam
    samtools view -bS -q 20 $sam > $bam
    samtools sort $bam ${bam_sort%.*}
    bamToBed -i $bam_sort > $bed

    #foreach i (*bed)
    bed_10m=${bed}_10m
    bedtools sample -n 10000000 -i $bed > ${bed}_10M
done


stages=(st9 st10 st11 st12 st13 st14 st15 st18 st21 st27)
for st in ${stages[@]}
do
    cat ${st}_rep1_CTCF_trim_q20_sort.bed_10M ${st}_rep2_CTCF_trim_q20_sort.bed_10M > ${st}_combined_CTCF.bed
    cat ${st}_rep1_input_trim_q20_sort.bed_10M ${st}_rep2_input_trim_q20_sort.bed_10M > ${st}_combined_input.bed
    macs2 callpeak -t ${st}_combined_CTCF.bed -c ${st}_combined_input.bed -f BED -g 600000000 --outdir ./ -n ${st}_combined_CTCF -q 0.01 -B --SPMR
done

