# About this repository
This repository contains a set of programs to reproduce the results in the main text of this paper.

> CTCF looping is established during gastrulation in medaka embryos
Ryohei Nakamura, Yuichi Motai, Masahiko Kumagai, Haruyo Nishiyama, Neva C. Durand, Kaori Kondo, Takashi Kondo, Tatsuya Tsukahara, Atsuko Shimada, Erez Lieberman Aiden, Shinichi Morishita, Hiroyuki Takeda
bioRxiv 454082; doi: https://doi.org/10.1101/454082

# Data download
Data access: All sequencing data generated in this study have been submitted to the DDBJ BioProject
database (https://www.ddbj.nig.ac.jp/index-e.html) under accession number PRJDB7492. Accession
number for raw sequencing data is DRA007417.

## Requirements
- dmd and rdmd https://dlang.org/
- GEM library tools https://sourceforge.net/projects/gemlibrary/
- MACS2 https://github.com/taoliu/MACS
- Trimmomatic http://www.usadellab.org/cms/?page=trimmomatic
- STAR
- BWA
- Picard
- bedtools
- GATK
- R
- python2
- samtools
- FIMO
- GPU and JCUDA
- Juicer
- Juicer Tools
- Sun Grid Engine

GPU and JCUDA were used for Juicer Commandline Tools HICCUPS.
Please check

It is assumed that Juicer runs on CentOS by using Sun Grid Engine.
For this environment, Juicer scripts for Univa Grid Engine is available in
the UGER directory. For more detail, see the official website
https://github.com/aidenlab/juicer/wiki/Running-Juicer-on-a-cluster.
You may have to modify the scripts for Sun Grid Engine.
We used Juicer version 1.5 for Arrowhead, HICCUPS, APA and dump,
and version 1.7.6 to calculate Pearson's correlation matrices and their eigenvectors.

# Run all programs
Firstly it is required to download this repository.

```sh
git clone https://tashiraka@bitbucket.org/tashiraka/medaka_hic.git
```

Then, you have to write the pathes of softwares and of the downloaded data on your enviroment into `run_all.sh` following the comments in the script. Finally the reproduction process becomes executable. 

```sh
cd medaka_hic
bash run_all.sh
```

Note that the process described in run_all.sh are organized specially for the publication. It is possible that the programs do not run on your device because of environmental difference or bugs. So, please consider this repository just as a experiment notebook and modify the files for your enviroment.

# License
This software is released under the MIT License, see LICENSE.