#!/bin/bash

#
# Note that the process described in run_all.sh are organized specially
# for the publication. It is possible that the programs do not run on your
# device because of environmental difference or bugs. So, please consider
# this repository just as a experiment notebook and modify the files
# for your enviroment.
#

#
# Path to tools
#
trimmomatic_path="" # Trimmomatic version 0.32
picard_path="" # Picard version 2.6.0
gatk_path=""   # GATK version 3.6
juicer_dir=""  # Juicer version 1.5
juicer_tools="" # Juicer Tools version 1.5 for Arrowhead, HICCUPS, APA and dump.
                # Juicer Tools version 1.7.6 for Pearson's correlation matrices
                # and their eigenvectors.
juicer_tools_10="" # Juicer Tools version 1.5 for N10 statisics whose source
                   # codes of Arrowhead are modified to change the default
                   # filter size from 300kB to 50kB at 5kb resplution
                   # (60 to 10 bins).
jcuda_lib_path="" # JCUDA library path for HICCUPS

# It is assumed that Juicer runs on CentOS by using Sun Grid Engine.
# For this environment, Juicer scripts for Univa Grid Engine is available in
# the UGER directory. For more detail, see the official website
# https://github.com/aidenlab/juicer/wiki/Running-Juicer-on-a-cluster.
# You may have to modify the scripts for Sun Grid Engine.

#
# Path to data
#
# All biological data can be downloaded from DDBJ BioProject database
# (https://www.ddbj.nig.ac.jp/index-e.html) under accession number PRJDB7492.
# Accession number for raw sequencing data is DRA007417.
ctcf_fastqs=() # CTCF ChiP-seq fastqs. Assuming the file name pattern is
               # 'st(9|10|11|12|13|14|15|18|21|27)_rep(1|2)_(CTCF|input).fastq(.gz)?'.
h3k27ac_fastqs=() # downloaded from DDBJ. Assuming the file name pattern is
                  # 'st(9|10|10.5|11|11.5|12)_(H3K27ac|input).fastq(.gz)?'
atac_fastqs=() # downloaded from DDBJ. Assuming the file name pattern is
               # 'st(9|10|10.5|11|11.5|12)_ATAC.fastq(.gz)?'. 
hic_fastq_dir="" # containing the fastq files downloaded from DDBJ.
                 # Assuming the file name pattern is
                 # 'st(9|10|10.5|11|11.5|12|13|14|15|18|21|27|fibro)_rep(1|2)*.fastq(.gz)?'.
rna_fastq_dir="" # downloaded from DDBJ. Assuming the file name pattern is
                 # 'RNA_st(9|10|11|12|13|14|15|18|21|27)_rep(1|2).fastq(.gz)?'
hybrid_rna_sample_names=() # Download the fastq files into hybrid_RNA-seq/fastq.
                           # Assuming the file names are '*_(1|2).fastq'.
                           # hybrid_rna_sample_names stores the '*' part of 
                           # those names.

#
# Processing
#
# Preparing reference genome
cd genome
bash genome.sh $juicer_path
cd ..

# CTCF ChIP-seq
cd CTCF_ChIP-seq
bash ctcf_chip.sh $trimmomatic_path ${ctcf_fastqs[@]}
cd ..

# Hi-C
cd Hi-C
bash hic.sh $juicer_dir $juicer_tools $juicer_tools_10 $jcuda_lib_path $hic_fastq_dir
cd ..

# ATAC-seq
cd ATAC-seq
bash atac.sh $trimmomatic_path ${atac_fastqs[@]}
cd ..

# H3K27ac ChIP-seq
cd H3K27ac_ChIP-seq
bash h3k27ac_chip.sh $trimmomatic_path ${h3k27ac_fastqs[@]}
cd ..

# RNA-seq
cd RNA-seq
bash rna.sh $trimmomatic_path
cd ..

# RNA-seq of hybrid medaka
cd hybrid_RNA-seq
bash hybrid_rna.sh $trimmomatic_path $picard_path $gatk_path ${hybrid_rna_sample_names[@]}
cd ..
